import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Constants {
  static const String NOTE_ROOT_DIR = ".NOTEROOTFOLDER";

  static const Color subTitleColor = Color.fromRGBO(201, 157, 62, 1.0);
  static const Color notePointBackColor = Color.fromRGBO(255, 255, 157, 0.0);
  static const Color notePointBorderColor = Color.fromRGBO(201, 157, 62, 1.0);
  static const Color notePointActionShadowColor =
      Color.fromRGBO(255, 206, 109, 1.0);
  static const Color notePointTextColor = Colors.black;

  static TextStyle pointTextStyle = GoogleFonts.robotoSlab(
    fontSize: 15.0,
    height: 1.5,
    color: notePointTextColor,
  );
}
