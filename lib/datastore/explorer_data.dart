import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';

import 'explorer_data_model.dart';

class ExplorerDataStore {
  static const EXPLORER_DATA_STORE_DIRECTORY = "explorer";
  static const EXPLORER_DATA_STORE_NAME = "explorer.json";

  File explorerStore;
  bool storeExists = false;

  static final ExplorerDataStore _explorerDataStore =
      ExplorerDataStore._internal();

  factory ExplorerDataStore() => _explorerDataStore;

  ExplorerDataStore._internal();

  Future<bool> initDataStore() async {
    try {
      Directory dir = await getExternalStorageDirectory();
      Directory storeDir =
          await Directory('${dir.path}/$EXPLORER_DATA_STORE_DIRECTORY/')
              .create(recursive: true);

      print(storeDir.path);

      explorerStore = File('${storeDir.path}$EXPLORER_DATA_STORE_NAME');
      storeExists = explorerStore.existsSync();
      if (storeExists) {
        readStore();
      } else {
        createStore();
      }
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  readStore() {
    if (explorerStore != null) {
      ExplorerDataModel data = ExplorerDataModel.fromJson(
          jsonDecode(explorerStore.readAsStringSync()));
      return data;
    }
    return ExplorerDataModel([], []);
  }

  createStore() {
    explorerStore.createSync();

    storeExists = true;

    writeStore(ExplorerDataModel([], []));
  }

  writeStore(ExplorerDataModel model) async {
    if (!storeExists) {
      createStore();
    }

    explorerStore.writeAsStringSync(jsonEncode(model));
  }
}
