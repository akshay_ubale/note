import 'package:note/models/note_metadata.dart';

class ExplorerDataModel {
  List<NoteMetadata> noteMetadatas;
  List<String> paths;

  ExplorerDataModel(
    List<NoteMetadata> noteMetadatas,
    List<String> paths,
  ) {
    this.noteMetadatas = noteMetadatas;
    this.paths = paths;
  }

  ExplorerDataModel.fromJson(Map<String, dynamic> json)
      : noteMetadatas = (json['noteMetadatas'] as List)
            .map((e) => NoteMetadata.fromJson(e))
            .toList(),
        paths = List<String>.from(json['paths']);

  Map<String, dynamic> toJson() => {
        'noteMetadatas': noteMetadatas,
        'paths': paths,
      };
}
