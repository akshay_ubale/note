import 'dart:convert';
import 'dart:io';

import 'package:note/datastore/note_data_store_model.dart';
import 'package:note/models/note_metadata.dart';
import 'package:path_provider/path_provider.dart';

class NoteDataStore {
  static const NOTE_DATA_STORE_DIRECTORY = "note";

  File noteStore;
  Directory noteDir;
  bool noteExists = false;

  static final NoteDataStore _noteDataStore = NoteDataStore._internal();

  factory NoteDataStore() => _noteDataStore;

  NoteDataStore._internal();

  Future<bool> initNoteStore() async {
    try {
      Directory dir = await getExternalStorageDirectory();
      noteDir = await Directory('${dir.path}/$NOTE_DATA_STORE_DIRECTORY/')
          .create(recursive: true);

      print(noteDir.path);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<bool> changeNote(NoteMetadata noteMetadata) async {
    noteStore = File('${noteDir.path}${noteMetadata.noteId}.json');
    noteExists = noteStore.existsSync();

    if (noteExists) {
      return true;
    }
    return createStore(noteMetadata);
  }

  bool createStore(NoteMetadata noteMetadata) {
    try {
      noteStore = File('${noteDir.path}${noteMetadata.noteId}.json');

      if (noteStore.existsSync()) {
        return true;
      }

      noteStore.createSync();

      writeStore(NoteDataStoreModel(
        noteSubTopics: [],
      ));
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  NoteDataStoreModel readStore() {
    if (noteStore != null) {
      NoteDataStoreModel note =
          NoteDataStoreModel.fromJson(jsonDecode(noteStore.readAsStringSync()));

      return note;
    }
    return null;
  }

  void writeStore(NoteDataStoreModel note) {
    noteStore.writeAsStringSync(jsonEncode(note));
  }

  void deleteNoteFile(NoteMetadata noteMetadata) {
    File deleteFile = File('${noteDir.path}${noteMetadata.noteId}.json');

    if (!deleteFile.existsSync()) {
      deleteFile.deleteSync();
    }
  }
}
