import 'package:flutter/cupertino.dart';
import 'package:note/models/note_sub_topic.dart';

class NoteDataStoreModel {
  List<NoteSubTopic> noteSubTopics;

  NoteDataStoreModel({@required this.noteSubTopics});

  NoteDataStoreModel.fromJson(Map<String, dynamic> json)
      : noteSubTopics = (json['noteSubTopics'] as List)
            .map((e) => NoteSubTopic.fromJson(e))
            .toList();

  Map<String, dynamic> toJson() => {
        'noteSubTopics': noteSubTopics,
      };
}
