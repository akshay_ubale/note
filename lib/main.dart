import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:note/redux/create_store.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:note/widgets/childrens/explorer/note_explorer_screen.dart';
import 'package:redux/redux.dart';

void main() async {
  final Store<NoteState> store = await createStore();

  print("initialState ${store.state.toString()}");
  WidgetsFlutterBinding.ensureInitialized();
  runApp(StoreProvider<NoteState>(
    store: store,
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My App",
      theme: ThemeData.dark(),
      home: MyNoteExplorerScreen(),
    );
  }
}

// getImage() async {
//   File image = await FilePicker.getFile(type: FileType.image);
//   print(image);
// }
