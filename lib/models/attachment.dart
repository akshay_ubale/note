enum AttachmentType { IMAGE, FILE }

class Attachment {
  AttachmentType attachmentType;
  String uri;

  Attachment({this.attachmentType, this.uri});

  @override
  String toString() {
    return "${this.attachmentType.toString()} : ${this.uri}";
  }

  Attachment.fromJson(Map<String, dynamic> json)
      : attachmentType = AttachmentType.values[json['attachmentType']],
        uri = json['uri'];

  Map<String, dynamic> toJson() => {
        'attachmentType': attachmentType.index,
        'uri': uri,
      };

  bool isEqual(Attachment obj) {
    return obj.attachmentType == attachmentType && obj.uri == uri;
  }
}
