import 'package:note/models/note_sub_topic.dart';

class Note {
  String noteId;
  String noteName;
  List<NoteSubTopic> noteSubTopics;

  Note({this.noteId, this.noteName, this.noteSubTopics});

  @override
  String toString() {
    return "\n NOTE : ${this.noteId}  ${this.noteName} \n ${this.noteSubTopics} \n";
  }

  Note.fromJson(Map<String, dynamic> json)
      : noteId = json['noteId'],
        noteName = json['noteName'],
        noteSubTopics = (json['noteSubTopics'] as List)
            .map((e) => NoteSubTopic.fromJson(e))
            .toList();

  Map<String, dynamic> toJson() => {
        'noteId': noteId,
        'noteName': noteName,
        'noteSubTopics': noteSubTopics,
      };
}
