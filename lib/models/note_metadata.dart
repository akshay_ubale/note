class NoteMetadata {
  String noteId;
  String path;
  String noteName;

  NoteMetadata(this.noteId, this.path, this.noteName);

  @override
  String toString() {
    return "${this.noteId} : ${this.noteName} : ${this.path}";
  }

  NoteMetadata.fromJson(Map<String, dynamic> json)
      : noteId = json['noteId'],
        path = json['path'],
        noteName = json['noteName'];

  Map<String, dynamic> toJson() => {
        'noteId': noteId,
        'path': path,
        'noteName': noteName,
      };

  bool operator ==(old) =>
      old is NoteMetadata &&
      old.noteId == noteId &&
      old.path == path &&
      old.noteName == noteName;

  @override
  int get hashCode => noteId.hashCode ^ noteName.hashCode ^ path.hashCode;
}
