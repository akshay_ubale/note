import 'package:note/models/attachment.dart';

enum PointType { TEXT, MEDIA }

class NotePoint {
  String id;
  String text;
  List<Attachment> attachments;
  PointType pointType;

  NotePoint({this.id, this.text, this.pointType, this.attachments});

  @override
  String toString() {
    return "${this.pointType.toString()} : ${this.text} : ${this.attachments}";
  }

  NotePoint.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        text = json['text'],
        attachments = (json['attachments'] as List)
            .map((e) => Attachment.fromJson(e))
            .toList(),
        pointType = PointType.values[json['pointType']];

  Map<String, dynamic> toJson() => {
        'id': id,
        'text': text,
        'attachments': attachments,
        'pointType': pointType.index,
      };

  bool isEmpty() {
    if (text.isEmpty && attachments.length <= 0) {
      return true;
    }
    return false;
  }

  bool isEqual(NotePoint obj) {
    if (obj.id != id || obj.text != text || obj.pointType != pointType) {
      return false;
    }

    if (obj.attachments.length != attachments.length) {
      return false;
    }

    for (int i = 0; i < obj.attachments.length; i++) {
      if (!obj.attachments[i].isEqual(this.attachments[i])) {
        return false;
      }
    }
    return true;
  }
}
