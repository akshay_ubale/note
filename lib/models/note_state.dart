import 'package:note/models/note.dart';
import 'package:note/models/note_metadata.dart';

class NoteState2 {
  //Explorer State
  List<NoteMetadata> noteMetadatas;
  List<String> paths;
  String activePath;
  List<String> activeFolders;
  List<NoteMetadata> activeNotes;

  //Note State
  NoteMetadata activeNoteMetadata;
  Note note;

  //Errors
  String error;

  NoteState2({
    this.noteMetadatas,
    this.paths,
    this.activePath,
    this.activeNoteMetadata,
    this.note,
    this.activeFolders,
    this.activeNotes,
    this.error,
  });

  NoteState2.fromNoteState(NoteState2 old) {
    noteMetadatas = old.noteMetadatas;
    paths = old.paths;
    activePath = old.activePath;
    activeNoteMetadata = old.activeNoteMetadata;
    note = old.note;
    activeFolders = old.activeFolders;
    activeNotes = old.activeNotes;
    error = old.error;
  }

  @override
  String toString() {
    return "NoteMetadatas: ${this.noteMetadatas} \n" +
        "Paths: ${this.paths} \n" +
        "ActivePath: ${this.activePath} \n" +
        "ActiveNoteMetadatas ${this.activeNoteMetadata} \n" +
        "Notes ${this.note} \n" +
        "ActiveFolders ${this.activeFolders} \n" +
        "ActiveNotes ${this.activeNotes} \n" +
        "Error ${this.error} \n";
  }
}
