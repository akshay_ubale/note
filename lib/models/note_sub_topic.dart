import 'package:note/models/note_point.dart';

class NoteSubTopic {
  String topicId;
  String subTopicName;
  List<NotePoint> notePoints;

  NoteSubTopic({this.topicId, this.subTopicName, this.notePoints});

  @override
  String toString() {
    return "NOTE SUB TOPIC : ${this.topicId} : ${this.subTopicName} : ${this.notePoints} \n";
  }

  NoteSubTopic.fromJson(Map<String, dynamic> json)
      : topicId = json['topicId'],
        subTopicName = json['subTopicName'],
        notePoints = (json['notePoints'] as List)
            .map((e) => NotePoint.fromJson(e))
            .toList();

  Map<String, dynamic> toJson() => {
        'topicId': topicId,
        'subTopicName': subTopicName,
        'notePoints': notePoints,
      };

  void updateNotePoint(NotePoint notePoint) {
    this.notePoints.forEach(
      (point) {
        if (point.id == notePoint.id) {
          point.text = notePoint.text;
          point.attachments = notePoint.attachments;
        }
      },
    );
  }

  bool isEqual(NoteSubTopic obj) {
    if (obj.topicId != topicId || obj.subTopicName != subTopicName) {
      return false;
    }

    if (obj.notePoints.length != notePoints.length) {
      return false;
    }

    for (int i = 0; i < obj.notePoints.length; i++) {
      if (!obj.notePoints[i].isEqual(this.notePoints[i])) {
        return false;
      }
    }

    return true;
  }
}
