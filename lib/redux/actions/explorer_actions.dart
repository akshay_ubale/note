import 'package:note/datastore/explorer_data_model.dart';
import 'package:note/models/note_metadata.dart';

class LoadExporerData {
  LoadExporerData();
}

class LoadExplorerDataSuccess {
  final ExplorerDataModel explorerDataModel;

  LoadExplorerDataSuccess(this.explorerDataModel);
}

class SyncWithDatabase {
  SyncWithDatabase();
}

class UpdateActivePath {
  final String newActivePath;

  UpdateActivePath(this.newActivePath);
}

class GoBackOneFolder {
  GoBackOneFolder();
}

class GoFrontOneFolder {
  final String folder;

  GoFrontOneFolder(this.folder);
}

class CreateFolderToDB {
  final String folderName;

  CreateFolderToDB(this.folderName);
}

class CreateFolderToDBSuccess {
  final String newFolder;
  final String newPath;

  CreateFolderToDBSuccess(this.newFolder, this.newPath);
}

class CreateNoteToDB {
  final String noteName;

  CreateNoteToDB(this.noteName);
}

class CreateNoteToDBSuccess {
  final NoteMetadata noteMetadata;

  CreateNoteToDBSuccess(this.noteMetadata);
}

class DeleteFolder {
  final String folderName;

  DeleteFolder(this.folderName);
}

class UpdateFolder {
  final String oldFolder;
  final String newFolder;

  UpdateFolder(this.oldFolder, this.newFolder);
}

class DeleteNote {
  final NoteMetadata noteMetadata;

  DeleteNote(this.noteMetadata);
}

class MoveNoteToFolder {
  final String newPath;
  final NoteMetadata noteMetadata;

  MoveNoteToFolder(this.newPath, this.noteMetadata);
}

class UpdateNoteName {
  final NoteMetadata noteMetadata;
  final String newName;

  UpdateNoteName(this.noteMetadata, this.newName);
}

class ResetChanged {
  ResetChanged();
}
