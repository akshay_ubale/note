import 'dart:io';

import 'package:note/models/note.dart';
import 'package:note/models/note_metadata.dart';
import 'package:note/models/note_point.dart';
import 'package:note/models/note_sub_topic.dart';

class LoadNotes {
  final NoteMetadata noteMetadata;

  LoadNotes(this.noteMetadata);
}

class UpdateTextPoint {
  final String topicId;
  final String pointId;
  final String pointValue;

  UpdateTextPoint(this.topicId, this.pointId, this.pointValue);
}

class UpdatePoint {
  final NotePoint point;
  final String topicId;

  UpdatePoint(this.topicId, this.point);
}

class UpdateNotePoints {
  final List<NotePoint> notePoints;
  final String topicId;

  UpdateNotePoints(this.topicId, this.notePoints);
}

class AddTextPoint {
  final String topicId;
  final String selectedId;

  AddTextPoint(this.topicId, this.selectedId);
}

class AddPointSuccess {
  final NotePoint notePoint;
  final String topicId;
  final String selectedId;

  AddPointSuccess(this.topicId, this.selectedId, this.notePoint);
}

class AddMediaPoint {
  final String topicId;
  final String selectedId;
  final File file;

  AddMediaPoint(this.topicId, this.selectedId, this.file);
}

class DeletePoint {
  final String topicId;
  final String pointId;

  DeletePoint(this.topicId, this.pointId);
}

class AddSubTopic {
  final String topicName;

  AddSubTopic(this.topicName);
}

class AddSubTopicSuccess {
  final NoteSubTopic noteSubTopic;

  AddSubTopicSuccess(this.noteSubTopic);
}

class LoadNoteSuccess {
  final NoteMetadata noteMetadata;
  final Note note;

  LoadNoteSuccess(this.noteMetadata, this.note);
}

class SyncNoteToDB {
  SyncNoteToDB();
}

class AddAnonimousSubTopic {
  NotePoint notePoint;

  AddAnonimousSubTopic(this.notePoint);
}

class ResetChanged {
  ResetChanged();
}
