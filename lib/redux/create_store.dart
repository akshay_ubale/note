import 'package:note/redux/reducers/app_reducer.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:redux/redux.dart';

Future<Store<NoteState>> createStore() async {
  return Store(
    appStateReducer,
    initialState: NoteState.initialState(),
  );
}
