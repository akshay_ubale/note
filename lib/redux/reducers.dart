// import 'dart:math';

// import 'package:note/Constants/contants.dart';
// import 'package:note/redux/state/app_state.dart';
// import 'package:redux/redux.dart';

// import 'actions/actions.dart';
// import 'actions/explorer_actions.dart';
// import 'actions/note_actions.dart';

// final noteReducers = combineReducers<NoteState>([
//   TypedReducer<NoteState, UpdateActivePath>(updateActivePath),
//   TypedReducer<NoteState, LoadNoteSuccess>(loadNoteSuccess),
//   TypedReducer<NoteState, CreateFolderToDB>(createFolderToDB),
//   TypedReducer<NoteState, CreateNoteToDBSuccess>(createNoteToDBSuccess),
//   TypedReducer<NoteState, DeleteFolder>(deleteFolder),
//   TypedReducer<NoteState, UpdateFolder>(updateFolder),
//   TypedReducer<NoteState, DeleteNote>(deleteNote),
//   TypedReducer<NoteState, MoveNoteToFolder>(moveNote),
//   TypedReducer<NoteState, UpdateNoteName>(updateNoteName),
//   TypedReducer<NoteState, ActionFailure>(onExplorerError),
//   TypedReducer<NoteState, RemoveError>(removeError),
//   TypedReducer<NoteState, UpdateTextPoint>(updateTextPoint),
//   TypedReducer<NoteState, AddPointSuccess>(addPointSuccess),
//   TypedReducer<NoteState, UpdateNotePoints>(updateNotePoints),
//   TypedReducer<NoteState, DeletePoint>(deletePoint),
//   TypedReducer<NoteState, UpdatePoint>(updatePoint),
//   TypedReducer<NoteState, AddSubTopicSuccess>(addSubTopic),
//   TypedReducer<NoteState, LoadExplorerDataSuccess>(loadExplorerDataSuccess),
// ]);

// NoteState loadExplorerDataSuccess(
//     NoteState noteState, LoadExplorerDataSuccess action) {
//   noteState.noteMetadatas = action.explorerDataModel.noteMetadatas;
//   noteState.paths = action.explorerDataModel.paths;

//   return updateActivePath(noteState, UpdateActivePath(Constants.NOTE_ROOT_DIR));
// }

// NoteState addSubTopic(NoteState noteState, AddSubTopicSuccess action) {
//   noteState.note.noteSubTopics.add(action.noteSubTopic);

//   return NoteState.fromNoteState(noteState);
// }

// NoteState updatePoint(NoteState noteState, UpdatePoint action) {
//   noteState.note.noteSubTopics
//       .firstWhere((subTopic) => subTopic.topicId == action.topicId)
//       .updateNotePoint(action.point);

//   return NoteState.fromNoteState(noteState);
// }

// NoteState deletePoint(NoteState noteState, DeletePoint action) {
//   noteState.note.noteSubTopics
//       .firstWhere((subTopic) => subTopic.topicId == action.topicId)
//       .notePoints
//       .removeWhere((point) => point.id == action.pointId);

//   return NoteState.fromNoteState(noteState);
// }

// NoteState updateNotePoints(NoteState noteState, UpdateNotePoints action) {
//   noteState.note.noteSubTopics
//       .firstWhere((subTopic) => subTopic.topicId == action.topicId)
//       .notePoints = action.notePoints;

//   return NoteState.fromNoteState(noteState);
// }

// NoteState addPointSuccess(NoteState noteState, AddPointSuccess action) {
//   int index = -1;
//   if (action.selectedId.isNotEmpty) {
//     index = noteState.note.noteSubTopics
//         .firstWhere((subTopic) => subTopic.topicId == action.topicId)
//         .notePoints
//         .indexWhere((subTopic) => subTopic.id == action.selectedId);
//   } else {
//     index = noteState.note.noteSubTopics
//             .firstWhere((subTopic) => subTopic.topicId == action.topicId)
//             .notePoints
//             .length -
//         1;
//   }
//   noteState.note.noteSubTopics
//       .firstWhere((subTopic) => subTopic.topicId == action.topicId)
//       .notePoints
//       .insert(index + 1, action.notePoint);

//   return NoteState.fromNoteState(noteState);
// }

// NoteState updateTextPoint(NoteState noteState, UpdateTextPoint action) {
//   noteState.note.noteSubTopics
//       .firstWhere((subTopic) => subTopic.topicId == action.topicId)
//       .notePoints
//       .forEach(
//     (point) {
//       if (point.id == action.pointId) {
//         point.text = action.pointValue;
//       }
//     },
//   );

//   return NoteState.fromNoteState(noteState);
// }

// NoteState removeError(NoteState noteState, RemoveError action) {
//   noteState.error = '';
//   return NoteState.fromNoteState(noteState);
// }

// NoteState onExplorerError(NoteState noteState, ActionFailure action) {
//   noteState.error = action.errorText;
//   return NoteState.fromNoteState(noteState);
// }

// NoteState deleteFolder(NoteState noteState, DeleteFolder action) {
//   String folderPathToRemove = noteState.activePath + "." + action.folderName;

//   noteState.paths.removeWhere((path) => path.startsWith(folderPathToRemove));

//   return updateActivePath(noteState, UpdateActivePath(noteState.activePath));
// }

// NoteState deleteNote(NoteState noteState, DeleteNote action) {
//   noteState.noteMetadatas
//       .removeWhere((noteMeta) => noteMeta.noteId == action.noteMetadata.noteId);

//   return updateActivePath(noteState, UpdateActivePath(noteState.activePath));
// }

// NoteState moveNote(NoteState noteState, MoveNoteToFolder action) {
//   noteState.noteMetadatas
//       .firstWhere((noteMeta) => noteMeta.noteId == action.noteMetadata.noteId)
//       .path = action.newPath;

//   return updateActivePath(noteState, UpdateActivePath(noteState.activePath));
// }

// NoteState updateNoteName(NoteState noteState, UpdateNoteName action) {
//   noteState.noteMetadatas
//       .firstWhere((noteMeta) => noteMeta.noteId == action.noteMetadata.noteId)
//       .noteName = action.newName;

//   return NoteState.fromNoteState(noteState);
// }

// NoteState updateFolder(NoteState noteState, UpdateFolder action) {
//   String oldPath = noteState.activePath + "." + action.oldFolder;
//   String newPath = noteState.activePath + "." + action.newFolder;

//   for (int i = 0; i < noteState.paths.length; i++) {
//     if (noteState.paths[i].startsWith(oldPath)) {
//       noteState.paths[i] = noteState.paths[i].replaceFirst(oldPath, "");
//       noteState.paths[i] = newPath + noteState.paths[i];
//     }
//   }

//   for (int i = 0; i < noteState.noteMetadatas.length; i++) {
//     if (noteState.noteMetadatas[i].path.startsWith(oldPath)) {
//       noteState.noteMetadatas[i].path =
//           noteState.noteMetadatas[i].path.replaceFirst(oldPath, "");
//       noteState.noteMetadatas[i].path =
//           newPath + noteState.noteMetadatas[i].path;
//     }
//   }

//   return updateActivePath(noteState, UpdateActivePath(noteState.activePath));
// }

// NoteState createNoteToDBSuccess(
//     NoteState noteState, CreateNoteToDBSuccess action) {
//   noteState.noteMetadatas.add(action.noteMetadata);
//   noteState.activeNotes.add(action.noteMetadata);

//   return NoteState.fromNoteState(noteState);
// }

// NoteState createFolderToDB(NoteState noteState, CreateFolderToDB action) {
//   final newPath = noteState.activePath + "." + action.folderName;
//   noteState.paths.add(newPath);
//   noteState.activeFolders
//       .add(newPath.substring(newPath.lastIndexOf(".") + 1, newPath.length));
//   return NoteState.fromNoteState(noteState);
// }

// NoteState loadNoteSuccess(
//     NoteState noteState, LoadNoteSuccess loadNotesAction) {
//   noteState.note = loadNotesAction.note;
//   noteState.activeNoteMetadata = loadNotesAction.noteMetadata;
//   return NoteState.fromNoteState(noteState);
// }

// NoteState updateActivePath(
//     NoteState noteState, UpdateActivePath updateActiveAction) {
//   print("UPDATE aCTIVE PATH AND FOLDERS, NOTE LIST");
//   //update active path.
//   noteState.activePath = updateActiveAction.newActivePath;
//   //update active folders.
//   noteState.activeFolders = noteState.paths
//       .where((path) =>
//           path.startsWith(noteState.activePath) && path != noteState.activePath)
//       .map((path) {
//         String removeActive = path.replaceFirst(noteState.activePath + ".", "");
//         return removeActive.substring(
//             0,
//             removeActive.contains(".")
//                 ? removeActive.indexOf(".")
//                 : removeActive.length);
//       })
//       .toSet()
//       .toList();

//   //update active folder notes.
//   noteState.activeNotes = noteState.noteMetadatas
//       .where((noteMeta) => noteMeta.path == noteState.activePath)
//       .toList();

//   return NoteState.fromNoteState(noteState);
// }
