import 'package:note/redux/reducers/explorer_reducer.dart';
import 'package:note/redux/reducers/note_reducer.dart';
import 'package:note/redux/state/app_state.dart';

NoteState appStateReducer(NoteState appState, action) {
  return NoteState(
    explorerState: explorerReducers(appState.explorerState, action),
    currentNoteState: noteReducers1(appState.currentNoteState, action),
  );
}
