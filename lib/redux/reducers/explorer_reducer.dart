import 'package:note/Constants/contants.dart';
import 'package:note/redux/actions/explorer_actions.dart';
import 'package:note/redux/state/explorer_state.dart';
import 'package:redux/redux.dart';

final explorerReducers = combineReducers<ExplorerState>([
  TypedReducer<ExplorerState, UpdateActivePath>(updateActivePath),
  TypedReducer<ExplorerState, CreateFolderToDB>(createFolderToDB),
  TypedReducer<ExplorerState, CreateNoteToDBSuccess>(createNoteToDBSuccess),
  TypedReducer<ExplorerState, DeleteFolder>(deleteFolder),
  TypedReducer<ExplorerState, UpdateFolder>(updateFolder),
  TypedReducer<ExplorerState, DeleteNote>(deleteNote),
  TypedReducer<ExplorerState, MoveNoteToFolder>(moveNote),
  TypedReducer<ExplorerState, UpdateNoteName>(updateNoteName),
  TypedReducer<ExplorerState, LoadExplorerDataSuccess>(loadExplorerDataSuccess),
  TypedReducer<ExplorerState, ResetChanged>(resetChanged),
]);

ExplorerState loadExplorerDataSuccess(
    ExplorerState explorerState, LoadExplorerDataSuccess action) {
  explorerState.noteMetadatas = action.explorerDataModel.noteMetadatas;
  explorerState.paths = action.explorerDataModel.paths;

  explorerState.changed = true;

  return updateActivePath(
      explorerState, UpdateActivePath(Constants.NOTE_ROOT_DIR));
}

ExplorerState deleteNote(ExplorerState explorerState, DeleteNote action) {
  explorerState.noteMetadatas
      .removeWhere((noteMeta) => noteMeta.noteId == action.noteMetadata.noteId);

  explorerState.changed = true;

  return updateActivePath(
      explorerState, UpdateActivePath(explorerState.activePath));
}

ExplorerState moveNote(ExplorerState explorerState, MoveNoteToFolder action) {
  explorerState.noteMetadatas
      .firstWhere((noteMeta) => noteMeta.noteId == action.noteMetadata.noteId)
      .path = action.newPath;

  explorerState.changed = true;

  return updateActivePath(
      explorerState, UpdateActivePath(explorerState.activePath));
}

ExplorerState updateNoteName(
    ExplorerState explorerState, UpdateNoteName action) {
  explorerState.noteMetadatas
      .firstWhere((noteMeta) => noteMeta.noteId == action.noteMetadata.noteId)
      .noteName = action.newName;

  explorerState.changed = true;

  return ExplorerState.fromExplorerState(explorerState);
}

ExplorerState updateFolder(ExplorerState explorerState, UpdateFolder action) {
  String oldPath = explorerState.activePath + "." + action.oldFolder;
  String newPath = explorerState.activePath + "." + action.newFolder;

  for (int i = 0; i < explorerState.paths.length; i++) {
    if (explorerState.paths[i].startsWith(oldPath)) {
      explorerState.paths[i] = explorerState.paths[i].replaceFirst(oldPath, "");
      explorerState.paths[i] = newPath + explorerState.paths[i];
    }
  }

  for (int i = 0; i < explorerState.noteMetadatas.length; i++) {
    if (explorerState.noteMetadatas[i].path.startsWith(oldPath)) {
      explorerState.noteMetadatas[i].path =
          explorerState.noteMetadatas[i].path.replaceFirst(oldPath, "");
      explorerState.noteMetadatas[i].path =
          newPath + explorerState.noteMetadatas[i].path;
    }
  }

  explorerState.changed = true;

  return updateActivePath(
      explorerState, UpdateActivePath(explorerState.activePath));
}

ExplorerState deleteFolder(ExplorerState explorerState, DeleteFolder action) {
  String folderPathToRemove =
      explorerState.activePath + "." + action.folderName;

  explorerState.paths
      .removeWhere((path) => path.startsWith(folderPathToRemove));

  explorerState.changed = true;

  return updateActivePath(
      explorerState, UpdateActivePath(explorerState.activePath));
}

ExplorerState createNoteToDBSuccess(
    ExplorerState explorerState, CreateNoteToDBSuccess action) {
  explorerState.noteMetadatas.add(action.noteMetadata);
  explorerState.activeNotes.add(action.noteMetadata);

  explorerState.changed = true;

  return ExplorerState.fromExplorerState(explorerState);
}

ExplorerState createFolderToDB(
    ExplorerState explorerState, CreateFolderToDB action) {
  final newPath = explorerState.activePath + "." + action.folderName;
  explorerState.paths.add(newPath);
  explorerState.activeFolders
      .add(newPath.substring(newPath.lastIndexOf(".") + 1, newPath.length));

  explorerState.changed = true;

  return ExplorerState.fromExplorerState(explorerState);
}

ExplorerState updateActivePath(
    ExplorerState explorerState, UpdateActivePath updateActiveAction) {
  print("UPDATE aCTIVE PATH AND FOLDERS, NOTE LIST");
  //update active path.
  explorerState.activePath = updateActiveAction.newActivePath;
  //update active folders.
  explorerState.activeFolders = explorerState.paths
      .where((path) =>
          path.startsWith(explorerState.activePath) &&
          path != explorerState.activePath)
      .map((path) {
        String removeActive =
            path.replaceFirst(explorerState.activePath + ".", "");
        return removeActive.substring(
            0,
            removeActive.contains(".")
                ? removeActive.indexOf(".")
                : removeActive.length);
      })
      .toSet()
      .toList();

  //update active folder notes.
  explorerState.activeNotes = explorerState.noteMetadatas
      .where((noteMeta) => noteMeta.path == explorerState.activePath)
      .toList();

  return ExplorerState.fromExplorerState(explorerState);
}

ExplorerState resetChanged(ExplorerState explorerState, ResetChanged action) {
  explorerState.changed = false;

  return ExplorerState.fromExplorerState(explorerState);
}
