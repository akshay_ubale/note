import 'package:note/models/note_sub_topic.dart';
import 'package:note/redux/actions/note_actions.dart';
import 'package:note/redux/state/note_state.dart';
import 'package:redux/redux.dart';

final noteReducers1 = combineReducers<SingleNoteState>([
  TypedReducer<SingleNoteState, LoadNoteSuccess>(loadNoteSuccess),
  TypedReducer<SingleNoteState, UpdateTextPoint>(updateTextPoint),
  TypedReducer<SingleNoteState, AddPointSuccess>(addPointSuccess),
  TypedReducer<SingleNoteState, UpdateNotePoints>(updateNotePoints),
  TypedReducer<SingleNoteState, DeletePoint>(deletePoint),
  TypedReducer<SingleNoteState, UpdatePoint>(updatePoint),
  TypedReducer<SingleNoteState, AddSubTopicSuccess>(addSubTopic),
  TypedReducer<SingleNoteState, ResetChanged>(resetChanged),
]);

SingleNoteState addSubTopic(
    SingleNoteState noteState, AddSubTopicSuccess action) {
  noteState.note.noteSubTopics.add(action.noteSubTopic);

  noteState.changed = true;

  return SingleNoteState.fromNoteState(noteState);
}

SingleNoteState updatePoint(SingleNoteState noteState, UpdatePoint action) {
  noteState.note.noteSubTopics
      .firstWhere((subTopic) => subTopic.topicId == action.topicId)
      .updateNotePoint(action.point);

  noteState.changed = true;

  return SingleNoteState.fromNoteState(noteState);
}

SingleNoteState deletePoint(SingleNoteState noteState, DeletePoint action) {
  NoteSubTopic noteSubTopic = noteState.note.noteSubTopics
      .firstWhere((subTopic) => subTopic.topicId == action.topicId);
  if (noteSubTopic.subTopicName.isEmpty) {
    noteState.note.noteSubTopics
        .removeWhere((subTopic) => subTopic.topicId == action.topicId);
  } else {
    noteState.note.noteSubTopics
        .firstWhere((subTopic) => subTopic.topicId == action.topicId)
        .notePoints
        .removeWhere((point) => point.id == action.pointId);
  }

  noteState.changed = true;

  return SingleNoteState.fromNoteState(noteState);
}

SingleNoteState updateNotePoints(
    SingleNoteState noteState, UpdateNotePoints action) {
  noteState.note.noteSubTopics
      .firstWhere((subTopic) => subTopic.topicId == action.topicId)
      .notePoints = action.notePoints;

  noteState.changed = true;

  return SingleNoteState.fromNoteState(noteState);
}

SingleNoteState addPointSuccess(
    SingleNoteState noteState, AddPointSuccess action) {
  int index = -1;
  if (action.selectedId.isNotEmpty) {
    index = noteState.note.noteSubTopics
        .firstWhere((subTopic) => subTopic.topicId == action.topicId)
        .notePoints
        .indexWhere((subTopic) => subTopic.id == action.selectedId);
  } else {
    index = noteState.note.noteSubTopics
            .firstWhere((subTopic) => subTopic.topicId == action.topicId)
            .notePoints
            .length -
        1;
  }
  noteState.note.noteSubTopics
      .firstWhere((subTopic) => subTopic.topicId == action.topicId)
      .notePoints
      .insert(index + 1, action.notePoint);

  noteState.changed = true;

  return SingleNoteState.fromNoteState(noteState);
}

SingleNoteState updateTextPoint(
    SingleNoteState noteState, UpdateTextPoint action) {
  noteState.note.noteSubTopics
      .firstWhere((subTopic) => subTopic.topicId == action.topicId)
      .notePoints
      .forEach(
    (point) {
      if (point.id == action.pointId) {
        point.text = action.pointValue;
      }
    },
  );

  noteState.changed = true;

  return SingleNoteState.fromNoteState(noteState);
}

SingleNoteState loadNoteSuccess(
    SingleNoteState noteState, LoadNoteSuccess loadNotesAction) {
  noteState.note = loadNotesAction.note;
  noteState.activeNoteMetadata = loadNotesAction.noteMetadata;
  return SingleNoteState.fromNoteState(noteState);
}

SingleNoteState resetChanged(SingleNoteState noteState, ResetChanged action) {
  noteState.changed = false;

  return SingleNoteState.fromNoteState(noteState);
}
