import 'package:flutter/cupertino.dart';
import 'package:note/redux/state/explorer_state.dart';
import 'package:note/redux/state/note_state.dart';

class NoteState {
  ExplorerState explorerState;
  SingleNoteState currentNoteState;

  NoteState({
    @required this.explorerState,
    @required this.currentNoteState,
  });

  NoteState.initialState()
      : explorerState = ExplorerState.initialState(),
        currentNoteState = SingleNoteState.initialState();
}
