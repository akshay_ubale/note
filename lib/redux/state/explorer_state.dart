import 'package:note/models/note_metadata.dart';

class ExplorerState {
  List<NoteMetadata> noteMetadatas;
  List<String> paths;
  String activePath;
  List<String> activeFolders;
  List<NoteMetadata> activeNotes;
  bool changed;

  ExplorerState({
    this.noteMetadatas,
    this.paths,
    this.activeFolders,
    this.activeNotes,
    this.activePath,
    this.changed,
  });

  ExplorerState.initialState()
      : activeFolders = [],
        paths = [],
        activeNotes = [],
        activePath = "",
        noteMetadatas = [],
        changed = false;

  ExplorerState.fromExplorerState(ExplorerState old) {
    noteMetadatas = old.noteMetadatas;
    paths = old.paths;
    activePath = old.activePath;
    activeFolders = old.activeFolders;
    activeNotes = old.activeNotes;
    changed = old.changed;
  }

  @override
  String toString() {
    return "NoteMetadatas: ${this.noteMetadatas} \n" +
        "Paths: ${this.paths} \n" +
        "ActivePath: ${this.activePath} \n" +
        "ActiveFolders ${this.activeFolders} \n" +
        "ActiveNotes ${this.activeNotes} \n" +
        "Changed ${this.changed} \n";
  }
}
