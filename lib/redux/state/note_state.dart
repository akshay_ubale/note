import 'package:note/models/note.dart';
import 'package:note/models/note_metadata.dart';

class SingleNoteState {
  NoteMetadata activeNoteMetadata;
  Note note;

  bool changed;

  SingleNoteState({
    this.activeNoteMetadata,
    this.note,
    this.changed,
  });

  SingleNoteState.fromNoteState(SingleNoteState old) {
    activeNoteMetadata = old.activeNoteMetadata;
    note = old.note;
    changed = old.changed;
  }

  SingleNoteState.initialState()
      : activeNoteMetadata = null,
        note = null,
        changed = false;

  @override
  String toString() {
    return "ActiveNoteMetadatas ${this.activeNoteMetadata} \n" +
        "Notes ${this.note} \n" +
        "Changed ${this.changed} \n";
  }
}
