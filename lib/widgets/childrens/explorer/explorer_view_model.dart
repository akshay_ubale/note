import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note/models/note_metadata.dart';
import 'package:note/redux/actions/actions.dart';
import 'package:note/redux/actions/explorer_actions.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:note/widgets/childrens/explorer/services/explorer_service.dart';
import 'package:note/widgets/childrens/note/display_note.dart';
import 'package:redux/redux.dart';

class ExplorerViewModel {
  final String activePath;
  final List<String> folders;
  final List<NoteMetadata> noteMetas;
  final bool changed;
  final Function(String) updateActivePath;
  final Function(BuildContext, NoteMetadata) redirectToNoteView;
  final Function(String) createFolder;
  final Function(String) createNote;
  final Function errorShown;
  final Function syncToDB;
  final Function(String) validateDelete;
  final Function(String) deleteFolder;
  final Function(String, String) updateFolder;
  final Function(NoteMetadata) deleteNote;
  final Function(NoteMetadata, String) updateNoteName;
  final Function(String) setActivePath;

  ExplorerViewModel({
    this.activePath,
    this.folders,
    this.noteMetas,
    this.changed,
    this.updateActivePath,
    this.redirectToNoteView,
    this.createFolder,
    this.createNote,
    this.errorShown,
    this.syncToDB,
    this.validateDelete,
    this.deleteFolder,
    this.updateFolder,
    this.deleteNote,
    this.updateNoteName,
    this.setActivePath,
  });

  factory ExplorerViewModel.create(Store<NoteState> store) {
    void _updateActivePath(String folder) {
      if (folder == "GOBACKONEFOLDER") {
        ExplorerService.goBackOneFolder(store);
      } else {
        ExplorerService.goFrontOneFolder(store, folder);
      }
    }

    void _createFolder(String folderName) {
      store.dispatch(CreateFolderToDB(folderName));
    }

    void _createNote(String noteName) {
      ExplorerService.createNote(store, noteName);
    }

    void _errorShown() {
      store.dispatch(RemoveError());
    }

    Future _redirectToNoteView(
        BuildContext context, NoteMetadata noteMetadata) async {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DisplayNote(noteMetadata),
        ),
      );
    }

    void _syncToDB() {
      ExplorerService.syncWithDatabase(store);
    }

    bool validateDeleteFolder(String folderName) {
      String path = store.state.explorerState.activePath + "." + folderName;

      int search = store.state.explorerState.noteMetadatas
          .indexWhere((element) => element.path.contains(path));

      if (search == -1) {
        return true;
      }
      return false;
    }

    void deleteFolder(String folderName) {
      store.dispatch(DeleteFolder(folderName));
    }

    void updateFolder(String oldP, String newP) {
      store.dispatch(UpdateFolder(oldP, newP));
    }

    void deleteNote(NoteMetadata noteMeta) {
      ExplorerService.deleteNote(store, noteMeta);
    }

    void updateNoteName(NoteMetadata noteMetadata, String newName) {
      store.dispatch(UpdateNoteName(noteMetadata, newName));
    }

    void setActivePath(String path) {
      store.dispatch(UpdateActivePath(path));
    }

    return ExplorerViewModel(
      activePath: store.state.explorerState.activePath,
      folders: store.state.explorerState.activeFolders,
      noteMetas: store.state.explorerState.activeNotes,
      changed: store.state.explorerState.changed,
      updateActivePath: _updateActivePath,
      redirectToNoteView: _redirectToNoteView,
      createFolder: _createFolder,
      createNote: _createNote,
      errorShown: _errorShown,
      syncToDB: _syncToDB,
      validateDelete: validateDeleteFolder,
      deleteFolder: deleteFolder,
      updateFolder: updateFolder,
      deleteNote: deleteNote,
      updateNoteName: updateNoteName,
      setActivePath: setActivePath,
    );
  }

  bool operator ==(old) {
    if (changed) {
      return false;
    }
    return (old is ExplorerViewModel &&
        equateFolders(old.folders) &&
        equateNoteMetas(old.noteMetas));
  }

  @override
  int get hashCode =>
      activePath.hashCode ^
      folders.hashCode ^
      noteMetas.hashCode ^
      changed.hashCode;

  bool equateFolders(List<String> folders) {
    if (folders.length != this.folders.length) {
      return false;
    }
    bool same = true;
    for (var i = 0; i < folders.length; i++) {
      if (folders[i] != this.folders[i]) {
        same = false;
      }
    }

    return same;
  }

  bool equateNoteMetas(List<NoteMetadata> noteMetas) {
    if (noteMetas.length != this.noteMetas.length) {
      return false;
    }

    bool same = true;
    for (var i = 0; i < noteMetas.length; i++) {
      if (noteMetas[i] != this.noteMetas[i]) {
        same = false;
      }
    }

    return same;
  }
}
