import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note/widgets/features/dialogs/single_input_dialog.dart';
import 'package:note/widgets/features/widgets/icon_tray.dart';

class FolderWidget extends StatelessWidget {
  final String folderName;
  final IconData folderIcon;
  final bool deletable;
  final Function(String) onDelete;
  final Function(String, String) onUpdate;
  final Function(String) validate;
  final bool showAction;

  FolderWidget({
    @required this.folderName,
    this.folderIcon,
    this.onDelete,
    this.validate,
    this.onUpdate,
    this.deletable = false,
    this.showAction = true,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(
        left: 20.0,
      ),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1.0,
            color: Colors.white,
          ),
        ),
      ),
      child: Row(
        children: <Widget>[
          Icon(folderIcon, size: 30.0),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 20.0),
              child: Text(folderName),
            ),
          ),
          IconTray(
            icons: getActions(context),
          )
        ],
      ),
    );
  }

  List<IconButton> getActions(BuildContext context) {
    if (!showAction) {
      return [];
    }

    List<IconButton> icons = [
      IconButton(
        icon: Icon(Icons.edit),
        onPressed: () {
          showDialog(
            context: context,
            child: SingleInputDialog(
              dialogFor: "NOTE",
              onOkPressed: (val) {
                onUpdate(folderName, val);
              },
              validate: (val) {
                if (val == folderName) {
                  return true;
                }
                return validate(val);
              },
              value: folderName,
            ),
          );
        },
      )
    ];

    if (deletable) {
      icons.add(
        IconButton(
          icon: Icon(Icons.delete),
          onPressed: () {
            onDelete(folderName);
          },
        ),
      );
    }

    return icons;
  }
}
