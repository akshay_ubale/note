import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:note/models/note_metadata.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:note/widgets/features/dialogs/single_input_dialog.dart';
import 'package:note/widgets/features/move/move_dialog.dart';
import 'package:note/widgets/features/move/move_view_model.dart';
import 'package:note/widgets/features/widgets/icon_tray.dart';

class NoteWidget extends StatelessWidget {
  final NoteMetadata noteMetadata;
  final IconData noteIcon;
  final Function() onDelete;
  final Function(NoteMetadata, String) onUpdate;

  NoteWidget({
    @required this.noteMetadata,
    this.noteIcon,
    this.onDelete,
    this.onUpdate,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(
        left: 20.0,
      ),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 1.0,
            color: Colors.white,
          ),
        ),
      ),
      child: Row(
        children: <Widget>[
          Icon(noteIcon, size: 30.0),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 20.0),
              child: Text(
                noteMetadata.noteName,
                overflow: TextOverflow.ellipsis,
                softWrap: false,
                maxLines: 1,
              ),
            ),
          ),
          IconTray(
            icons: [
              IconButton(
                icon: Icon(Icons.arrow_upward),
                onPressed: () {
                  moveNote(context);
                },
              ),
              IconButton(
                icon: Icon(Icons.edit),
                onPressed: () {
                  showDialog(
                    context: context,
                    child: SingleInputDialog(
                      dialogFor: "FOLDER",
                      onOkPressed: (val) {
                        onUpdate(noteMetadata, val);
                      },
                      validate: (val) {
                        if (val == noteMetadata.noteName) {
                          return true;
                        }
                        return true;
                      },
                      value: noteMetadata.noteName,
                    ),
                  );
                },
              ),
              IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  onDelete();
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  void moveNote(BuildContext context) {
    showDialog(
      context: context,
      child: StoreConnector<NoteState, NoteMoveViewModel>(
        converter: (store) => NoteMoveViewModel.create(store),
        builder: (context, vm) {
          return NoteMoveDialog(
            noteMetadata: noteMetadata,
            activePath: vm.activePath,
            paths: vm.paths,
            onMove: vm.move,
          );
        },
      ),
    );
  }
}
