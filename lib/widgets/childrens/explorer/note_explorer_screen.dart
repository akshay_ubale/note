import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:note/Constants/contants.dart';
import 'package:note/datastore/explorer_data.dart';
import 'package:note/datastore/note_data_store.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:note/widgets/childrens/explorer/folder/folder_widget.dart';
import 'package:note/widgets/childrens/explorer/explorer_view_model.dart';
import 'package:note/widgets/childrens/explorer/folder/note_widget.dart';
import 'package:note/widgets/childrens/explorer/services/explorer_service.dart';
import 'package:note/widgets/features/dialogs/single_input_dialog.dart';
import 'package:note/widgets/features/widgets/file_path_line/file_history_widget.dart';
import 'package:redux/redux.dart';

class MyNoteExplorerScreen extends StatefulWidget {
  @override
  _NoteExplorerState createState() => _NoteExplorerState();
}

class _NoteExplorerState extends State<MyNoteExplorerScreen> {
  final _parentScaffoldKey = GlobalKey<ScaffoldState>();
  final ExplorerDataStore explorerDataStore = ExplorerDataStore();
  Store<NoteState> store;

  bool onRoot;

  @override
  void initState() {
    super.initState();
    onRoot = true;
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<NoteState, ExplorerViewModel>(
      distinct: true,
      onInit: (store) async {
        this.store = store;
        await NoteDataStore().initNoteStore();
        ExplorerDataStore().initDataStore().then((value) {
          if (value) {
            ExplorerService.loadExplorerData(store);
          }
        });
      },
      onDidChange: (viewModel) {
        print("I am changed");
        setState(() {
          onRoot =
              viewModel.activePath == Constants.NOTE_ROOT_DIR ? true : false;
        });

        viewModel.syncToDB();
      },
      converter: (store) => ExplorerViewModel.create(store),
      builder: (context, vm) {
        return WillPopScope(
          onWillPop: () {
            return onApplicationClose(vm.changed);
          },
          child: Scaffold(
            key: _parentScaffoldKey,
            appBar: AppBar(
              title: Text("Note"),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.sync),
                  onPressed: () {
                    vm.syncToDB();
                  },
                ),
                IconButton(
                  icon: Icon(Icons.note_add),
                  onPressed: () {
                    showInputDialog(context, vm, "Note");
                  },
                ),
                IconButton(
                  icon: Icon(Icons.create_new_folder),
                  onPressed: () {
                    showInputDialog(context, vm, "Folder");
                  },
                )
              ],
              leading: !onRoot
                  ? IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                      ),
                      onPressed: () {
                        vm.updateActivePath("GOBACKONEFOLDER");
                      },
                    )
                  : null,
            ),
            body: Container(
              child: Column(
                children: <Widget>[
                  FileHistoryWidget(
                    path: vm.activePath,
                    onPress: (path) {
                      vm.setActivePath(path);
                    },
                  ),
                  getExplorerBody(vm),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  List<Widget> getFolderWidgets(ExplorerViewModel vm) {
    return vm.folders.map((folder) {
      return InkWell(
        onTap: () {
          vm.updateActivePath(folder);
        },
        child: FolderWidget(
          folderName: folder,
          folderIcon: Icons.folder,
          deletable: vm.validateDelete(folder),
          onDelete: vm.deleteFolder,
          validate: (folderName) {
            return vm.folders.indexWhere((folder) => folder == folderName) == -1
                ? true
                : false;
          },
          onUpdate: vm.updateFolder,
        ),
      );
    }).toList();
  }

  List<Widget> getNotesFolderWidgets(
      BuildContext context, ExplorerViewModel vm) {
    return vm.noteMetas.map((noteMD) {
      return InkWell(
        onTap: () {
          vm.redirectToNoteView(context, noteMD);
        },
        child: NoteWidget(
          noteMetadata: noteMD,
          noteIcon: Icons.note,
          onDelete: () {
            vm.deleteNote(noteMD);
          },
          onUpdate: vm.updateNoteName,
        ),
      );
    }).toList();
  }

  Widget getExplorerBody(ExplorerViewModel viewModel) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: 25.0,
              padding: EdgeInsets.only(
                right: 20.0,
                left: 20.0,
              ),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.white,
                    width: 2.0,
                  ),
                  top: BorderSide(
                    color: Colors.white,
                    width: 2.0,
                  ),
                ),
                color: Colors.green,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "FOLDERS",
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 5.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Icon(
                        Icons.arrow_drop_down,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                children: viewModel.folders.length > 0
                    ? getFolderWidgets(viewModel)
                    : [
                        GestureDetector(
                          onTap: () {
                            showInputDialog(context, viewModel, "Folder");
                          },
                          child: Container(
                            height: 40.0,
                            alignment: Alignment.center,
                            child: Text(
                              "No Folders Created.",
                              style: GoogleFonts.roboto(
                                fontSize: 20.0,
                                color: Color.fromRGBO(255, 255, 255, 0.5),
                              ),
                            ),
                          ),
                        ),
                      ],
              ),
            ),
            Container(
              height: 25.0,
              padding: EdgeInsets.only(
                right: 20.0,
                left: 20.0,
              ),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.white,
                    width: 2.0,
                  ),
                  top: BorderSide(
                    color: Colors.white,
                    width: 2.0,
                  ),
                ),
                color: Colors.green,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "NOTES",
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 5.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Icon(
                        Icons.arrow_drop_down,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Column(
                children: viewModel.noteMetas.length > 0
                    ? getNotesFolderWidgets(context, viewModel)
                    : [
                        GestureDetector(
                          onTap: () {
                            showInputDialog(context, viewModel, "Note");
                          },
                          child: Container(
                            height: 40.0,
                            alignment: Alignment.center,
                            child: Text(
                              "No Notes Created.",
                              style: GoogleFonts.roboto(
                                fontSize: 20.0,
                                color: Color.fromRGBO(255, 255, 255, 0.5),
                              ),
                            ),
                          ),
                        ),
                      ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showInputDialog(
      BuildContext context, ExplorerViewModel vm, String forWhat) {
    showDialog(
      context: context,
      builder: (context) {
        if (forWhat == "Note") {
          return SingleInputDialog(
            onOkPressed: vm.createNote,
            dialogFor: forWhat,
            validate: (noteName) {
              return vm.noteMetas.indexWhere(
                          (noteMeta) => noteMeta.noteName == noteName) ==
                      -1
                  ? true
                  : false;
            },
          );
        } else if (forWhat == "Folder") {
          return SingleInputDialog(
            onOkPressed: vm.createFolder,
            dialogFor: forWhat,
            validate: (folderName) {
              return vm.folders.indexWhere((folder) => folder == folderName) ==
                      -1
                  ? true
                  : false;
            },
          );
        } else {
          return Dialog(
            child: Text("No Dialog Created"),
          );
        }
      },
    );
  }

  Future<bool> onApplicationClose(bool changed) async {
    if (!onRoot) {
      ExplorerService.goBackOneFolder(store);
      return false;
    }
    return true;
  }
}
