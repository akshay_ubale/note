import 'dart:math';

import 'package:note/Constants/contants.dart';
import 'package:note/datastore/explorer_data.dart';
import 'package:note/datastore/explorer_data_model.dart';
import 'package:note/datastore/note_data_store.dart';
import 'package:note/models/note_metadata.dart';
import 'package:note/redux/actions/explorer_actions.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:redux/redux.dart';

class ExplorerService {
  static void loadExplorerData(Store<NoteState> store) {
    try {
      ExplorerDataModel model = ExplorerDataStore().readStore();

      store.dispatch(LoadExplorerDataSuccess(model));
    } catch (e) {
      print("read exception " + e);
    }
  }

  static void deleteNote(Store<NoteState> store, NoteMetadata noteMetadata) {
    try {
      NoteDataStore().deleteNoteFile(noteMetadata);

      store.dispatch(DeleteNote(noteMetadata));
    } catch (e) {
      print(e);
    }
  }

  static void createNote(Store<NoteState> store, String noteName) {
    try {
      NoteMetadata noteMetadata = NoteMetadata(getUniqueId(noteName),
          store.state.explorerState.activePath, noteName);
      if (NoteDataStore().createStore(noteMetadata)) {
        store.dispatch(CreateNoteToDBSuccess(noteMetadata));
      }
    } catch (e) {
      print("Note Creation : " + e);
    }
  }

  static void goBackOneFolder(Store<NoteState> store) {
    String newActivePath = store.state.explorerState.activePath
        .substring(0, store.state.explorerState.activePath.lastIndexOf("."));
    if (newActivePath.isEmpty) {
      newActivePath = Constants.NOTE_ROOT_DIR;
    }
    store.dispatch(UpdateActivePath(newActivePath));
  }

  static void goFrontOneFolder(Store<NoteState> store, String folder) {
    String newActivePath = "";
    if (store.state.explorerState.activePath.isEmpty) {
      newActivePath = Constants.NOTE_ROOT_DIR;
    } else {
      newActivePath = store.state.explorerState.activePath + "." + folder;
    }

    store.dispatch(UpdateActivePath(newActivePath));
  }

  static void syncWithDatabase(Store<NoteState> store) {
    ExplorerDataModel data = ExplorerDataModel(
        store.state.explorerState.noteMetadatas,
        store.state.explorerState.paths);

    store.dispatch(ResetChanged());
    try {
      ExplorerDataStore().writeStore(data);
    } catch (e) {
      print(e);
    }
  }

  static String getUniqueId(String text) {
    return text.replaceAll(' ', '') +
        "AK" +
        DateTime.now().millisecondsSinceEpoch.toString() +
        "SH" +
        Random().nextInt(1000).toString();
  }
}
