import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:note/models/note_point.dart';
import 'package:note/widgets/childrens/note/containers/point/point_container_state.dart';
import 'package:note/widgets/childrens/note/containers/point/point_container_widget.dart';
import 'package:note/widgets/childrens/note/containers/text/text_widget.dart';
import 'package:note/widgets/features/widgets/marquee_text.dart';
import 'package:open_file/open_file.dart';
import 'package:path/path.dart';

class FileContainer extends PointContainerWidget {
  FileContainer({
    String id,
    Key key,
    bool selected,
    bool reading,
    NotePoint notePoint,
    Function(String) onSelect,
    Function(String) onDelete,
    Function(NotePoint) savePoint,
  }) : super(
          id: id,
          key: key,
          selected: selected,
          reading: reading,
          notePoint: notePoint,
          onSelect: onSelect,
          onDelete: onDelete,
          savePoint: savePoint,
        );

  @override
  _FileContainerState createState() => _FileContainerState();
}

class _FileContainerState extends PointContainerState<FileContainer> {
  @override
  Widget getWidget(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 50.0,
          margin:
              EdgeInsets.only(top: 10.0, bottom: 10.0, left: 10.0, right: 30.0),
          decoration: BoxDecoration(
            color: Colors.green[100],
            border: Border.all(
              color: Colors.green,
              width: 1.0,
            ),
          ),
          child: Row(
            children: <Widget>[
              Container(
                child: Icon(
                  Icons.picture_as_pdf,
                  size: 45.0,
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width - 100,
                child: MarqueeWidget(
                  child: Text(
                    basename(widget.notePoint.attachments[0].uri),
                    style: GoogleFonts.roboto(
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                    ),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              )
            ],
          ),
        ),
        TextWidget(
          onChange: (value) {
            handleTextUpdate(value);
          },
          text: widget.notePoint.text,
          disabled: widget.reading ?? false,
        )
      ],
    );
  }

  @override
  void onDoubleTap() {
    OpenFile.open(widget.notePoint.attachments[0].uri);
  }

  @override
  void onTap() {}
}
