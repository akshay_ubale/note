import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note/models/note_point.dart';
import 'package:note/widgets/childrens/note/containers/image/image_dialog.dart';
import 'package:note/widgets/childrens/note/containers/point/point_container_state.dart';
import 'package:note/widgets/childrens/note/containers/point/point_container_widget.dart';
import 'package:note/widgets/childrens/note/containers/text/text_widget.dart';

class ImageContainer extends PointContainerWidget {
  ImageContainer({
    String id,
    Key key,
    bool selected,
    bool reading,
    NotePoint notePoint,
    Function(String) onSelect,
    Function(String) onDelete,
    Function(NotePoint) savePoint,
  }) : super(
          id: id,
          key: key,
          selected: selected,
          reading: reading,
          notePoint: notePoint,
          onSelect: onSelect,
          onDelete: onDelete,
          savePoint: savePoint,
        );

  @override
  _ImageContainerState createState() => _ImageContainerState();
}

class _ImageContainerState extends PointContainerState<ImageContainer> {
  @override
  void onDoubleTap() {
    showDialog(
      context: context,
      child: ImageDialog(
        path: widget.notePoint.attachments[0].uri,
        onDelete: () {
          handleDeletion();
        },
      ),
    );
  }

  @override
  void onTap() {}

  @override
  Widget getWidget(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(5.0),
          child: Image.file(
            File(widget.notePoint.attachments[0].uri),
            height: 100,
            fit: BoxFit.fill,
            alignment: Alignment.center,
          ),
        ),
        TextWidget(
          text: widget.notePoint.text,
          onChange: (value) {
            handleTextUpdate(value);
          },
          disabled: widget.reading ?? false,
        ),
      ],
    );
  }
}
