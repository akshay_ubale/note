import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ImageDialog extends StatelessWidget {
  final String path;
  final Function() onDelete;

  ImageDialog({this.path, this.onDelete});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        height: 500,
        child: Column(
          children: <Widget>[
            Container(
              height: 50.0,
              child: IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  Navigator.of(context).pop();
                  onDelete();
                },
              ),
            ),
            Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: PhotoView(
                  imageProvider: FileImage(File(path)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
