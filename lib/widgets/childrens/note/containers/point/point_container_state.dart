import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note/Constants/contants.dart';
import 'package:note/widgets/childrens/note/containers/point/point_container_widget.dart';
import 'package:note/widgets/features/dialogs/delete_dialog.dart';

abstract class PointContainerState<Type extends PointContainerWidget>
    extends State<Type> {
  void onDoubleTap();
  void onTap();
  Widget getWidget(BuildContext context);

  void handleSelection() {
    widget.onSelect(widget.id);
  }

  void handleDeletion() {
    showDialog(
      context: context,
      child: DeleteDialog(
        confirmationText: 'Are you sure want to delete this Point ?',
        onOK: () {
          widget.onDelete(widget.id);
        },
      ),
    );
  }

  void handleTextUpdate(String newValue) {
    if (widget.notePoint.text != newValue) {
      widget.notePoint.text = newValue;
      widget.savePoint(widget.notePoint);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (details) {
        if (details.velocity.pixelsPerSecond.dx > 0.0) {
          handleDeletion();
        }
      },
      onTap: () {
        if (!widget.reading) {
          onTap();
        }
      },
      onDoubleTap: () {
        onDoubleTap();
      },
      onLongPress: () {
        if (!widget.reading) {
          handleSelection();
        }
      },
      child: Container(
        key: widget.key,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: widget.selected ? Colors.black12 : Colors.transparent,
          border: Border(
            left: BorderSide(
              width: 5.0,
              color: Constants.notePointBorderColor,
              style: BorderStyle.solid,
            ),
            right: BorderSide(
              width: 5.0,
              color: Constants.notePointBorderColor,
              style: BorderStyle.solid,
            ),
            bottom: BorderSide(
              width: 0.5,
              color: Colors.white30,
              style: BorderStyle.solid,
            ),
          ),
        ),
        child: Stack(
          children: <Widget>[
            getWidget(context),
            Positioned(
              right: 0,
              top: 2,
              child: Material(
                elevation: 10.0,
                color: Colors.transparent,
                shadowColor: Colors.amber,
                child: widget.selected
                    ? IconButton(
                        icon: Icon(
                          Icons.attach_file,
                          color: Colors.green,
                          size: 40.0,
                        ),
                        onPressed: () {
                          //Add or Update file to a widget.
                        },
                      )
                    : Container(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
