import 'package:note/models/note_point.dart';
import 'package:note/redux/actions/note_actions.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:redux/redux.dart';

class PointContainerViewModel {
  final String topicId;
  final NotePoint notePoint;
  final Function(NotePoint) updatePoint;
  final Function(String) deletePoint;

  PointContainerViewModel({
    this.topicId,
    this.notePoint,
    this.updatePoint,
    this.deletePoint,
  });

  factory PointContainerViewModel.create(
      Store<NoteState> store, String topicId, NotePoint notePoint) {
    void _updatePoint(NotePoint notePoint) {
      store.dispatch(UpdatePoint(topicId, notePoint));
    }

    void _deletePoint(String pointId) {
      store.dispatch(DeletePoint(topicId, pointId));
    }

    return PointContainerViewModel(
      topicId: topicId,
      notePoint: notePoint,
      updatePoint: _updatePoint,
      deletePoint: _deletePoint,
    );
  }
}
