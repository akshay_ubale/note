import 'package:flutter/cupertino.dart';
import 'package:note/models/note_point.dart';

abstract class PointContainerWidget extends StatefulWidget {
  final String id;
  final Key key;
  final bool selected;
  final bool reading;
  final NotePoint notePoint;
  final Function(String) onSelect;
  final Function(String) onDelete;
  final Function(NotePoint) savePoint;

  PointContainerWidget({
    this.id,
    this.key,
    this.onDelete,
    this.onSelect,
    this.selected,
    this.notePoint,
    this.savePoint,
    this.reading,
  });
}
