import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note/models/note_point.dart';
import 'package:note/widgets/childrens/note/containers/point/point_container_state.dart';
import 'package:note/widgets/childrens/note/containers/point/point_container_widget.dart';
import 'package:note/widgets/childrens/note/containers/text/text_widget.dart';

class TextContainer extends PointContainerWidget {
  TextContainer({
    String id,
    Key key,
    bool selected,
    bool reading,
    NotePoint notePoint,
    Function(String) onSelect,
    Function(String) onDelete,
    Function(NotePoint) savePoint,
  }) : super(
          id: id,
          key: key,
          selected: selected,
          reading: reading,
          notePoint: notePoint,
          onSelect: onSelect,
          onDelete: onDelete,
          savePoint: savePoint,
        );

  @override
  _TextContainerState createState() => _TextContainerState();
}

class _TextContainerState extends PointContainerState<TextContainer> {
  @override
  void onDoubleTap() {}

  @override
  Widget getWidget(BuildContext context) {
    return TextWidget(
      text: widget.notePoint.text,
      onChange: (value) {
        handleTextUpdate(value);
      },
      disabled: widget.reading ?? false,
    );
  }

  @override
  void onTap() {}
}
