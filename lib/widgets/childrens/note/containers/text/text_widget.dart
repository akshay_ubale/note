import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_parsed_text/flutter_parsed_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:note/Constants/contants.dart';
import 'package:url_launcher/url_launcher.dart';

class TextWidget extends StatefulWidget {
  final String text;
  final bool disabled;
  final Function(String) onChange;

  TextWidget({this.text, this.disabled, this.onChange});

  @override
  TextWidgetState createState() => TextWidgetState();
}

class TextWidgetState extends State<TextWidget> {
  bool dirty;
  TextEditingController controller;
  Timer _debounce;
  FocusNode focusNode;

  @override
  void initState() {
    super.initState();
    dirty = false;
    controller = new TextEditingController(text: widget.text);
    controller.addListener(onTextChange);
    focusNode = FocusNode();
    focusNode.addListener(() {
      if (!focusNode.hasFocus) {
        if (_debounce?.isActive ?? false) _debounce.cancel();

        widget.onChange(controller.text);
        setState(() {
          dirty = false;
        });
      }
    });
  }

  @override
  void dispose() {
    controller.removeListener(onTextChange);
    controller.dispose();
    _debounce?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: widget.disabled,
      child: GestureDetector(
        onTap: () {
          setState(() {
            dirty = true;
          });
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(
            bottom: 5.0,
            left: 5.0,
            right: 5.0,
          ),
          child: getTextField(context),
        ),
      ),
    );
  }

  Widget getTextField(BuildContext context) {
    if (widget.text.isEmpty || dirty) {
      return getEditingField();
    }
    return getText();
  }

  Widget getText() {
    return ParsedText(
      text: widget.text,
      parse: <MatchText>[
        MatchText(
          type: ParsedType.EMAIL,
          style: GoogleFonts.robotoSlab(
            fontSize: 15.0,
            height: 1.5,
          ),
          onTap: (email) {
            print(email);
          },
        ),
        MatchText(
          type: ParsedType.URL,
          style: GoogleFonts.robotoSlab(
            fontSize: 15.0,
            height: 1.5,
            color: Colors.blue,
          ),
          onTap: _launchUrl,
        ),
      ],
      style: Constants.pointTextStyle,
    );
  }

  Widget getEditingField() {
    return Container(
      child: TextFormField(
        controller: controller,
        focusNode: focusNode,
        cursorColor: Constants.notePointTextColor,
        keyboardType: TextInputType.multiline,
        maxLines: null,
        autofocus: true,
        decoration: new InputDecoration(
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          contentPadding: EdgeInsets.only(left: 5.0),
          hintText: "Enter note text",
          hintStyle: Constants.pointTextStyle.apply(
            color: Color.fromRGBO(193, 193, 193, 1.0),
          ),
        ),
        style: Constants.pointTextStyle,
      ),
    );
  }

  _launchUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      print("COuld not launch URL: " + url);
    }
  }

  void onTextChange() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(milliseconds: 2500), () {
      print(controller.text);
      widget.onChange(controller.text);
      setState(() {
        dirty = false;
      });
    });
  }
}
