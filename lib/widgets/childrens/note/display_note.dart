import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:note/datastore/note_data_store.dart';
import 'package:note/models/note_metadata.dart';
import 'package:note/models/note_point.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:note/widgets/childrens/note/services/note_service.dart';
import 'package:note/widgets/childrens/note/subtopic/sub_topic_widget.dart';
import 'package:note/widgets/childrens/note/display_note_view_model.dart';
import 'package:note/widgets/features/dialogs/single_input_dialog.dart';

class DisplayNote extends StatefulWidget {
  final NoteMetadata noteMetadata;

  DisplayNote(this.noteMetadata);

  @override
  _DisplayNoteState createState() => _DisplayNoteState();
}

class _DisplayNoteState extends State<DisplayNote> {
  String selectedTopicId;
  bool reading;

  @override
  void initState() {
    selectedTopicId = "";
    reading = false;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _parentScaffoldKey = GlobalKey<ScaffoldState>();

    return StoreConnector<NoteState, DisplayNoteViewModel>(
      distinct: true,
      onInit: (store) async {
        bool value = await NoteDataStore().changeNote(widget.noteMetadata);
        if (value) {
          NoteService.loadNotes(store, widget.noteMetadata);
        } else {
          Navigator.pop(context);
        }
      },
      onDidChange: (viewModel) {
        print("Hey I am changed : " + viewModel.changed.toString());
        viewModel.syncNote();
      },
      converter: (store) => DisplayNoteViewModel.create(store),
      builder: (context, vm) => WillPopScope(
        onWillPop: () {
          return onBackPress(vm);
        },
        child: Scaffold(
          key: _parentScaffoldKey,
          appBar: AppBar(
            title: Text(vm.noteName),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.sync),
                onPressed: () {
                  vm.syncNote();
                },
              ),
              IconButton(
                icon: reading ? Icon(Icons.edit) : Icon(Icons.block),
                onPressed: () {
                  setState(() {
                    reading = !reading;
                  });
                },
              ),
            ],
          ),
          floatingActionButton: getSubTopicAdditionButtonFAB(vm),
          body: SingleChildScrollView(
            padding: EdgeInsets.only(bottom: 60.0),
            child: Column(
              children: [
                ...getSubTopicWidgets(context, vm),
              ],
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> getSubTopicWidgets(
      BuildContext context, DisplayNoteViewModel vm) {
    return vm.noteSubTopics.map((noteSubTopic) {
      return SubTopicWidget(
        childrens: noteSubTopic.notePoints,
        topicId: noteSubTopic.topicId,
        topicName: noteSubTopic.subTopicName,
        addNewTextPoint: vm.addNewTextPoint,
        updateNotePoints: vm.updateNotePoints,
        addMediaPoint: vm.addMediaPoint,
        reading: reading,
      );
    }).toList();
  }

  Widget getSubTopicAdditionButtonFAB(DisplayNoteViewModel viewModel) {
    return SpeedDial(
      child: Icon(
        Icons.add,
        size: 40.0,
      ),
      visible: !reading,
      closeManually: false,
      curve: Curves.bounceIn,
      overlayColor: Colors.white,
      overlayOpacity: 0.8,
      onOpen: () {
        print("Opened FAB");
      },
      onClose: () {
        print("closed FAB");
      },
      tooltip: 'Speed Dial',
      heroTag: 'speed-dial-dero-tag',
      backgroundColor: Colors.transparent,
      foregroundColor: Colors.green,
      elevation: 0.0,
      shape: CircleBorder(),
      children: [
        SpeedDialChild(
          child: Icon(
            Icons.note,
            color: Colors.red,
          ),
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          label: 'SubTopic',
          labelStyle: TextStyle(
            fontSize: 18.0,
            color: Colors.black,
          ),
          onTap: () {
            showSubTopicTitleDislog(viewModel);
          },
        ),
        SpeedDialChild(
          child: Icon(
            Icons.text_fields,
            color: Colors.cyanAccent,
          ),
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          label: 'Text Point',
          labelStyle: TextStyle(
            fontSize: 18.0,
            color: Colors.black,
          ),
          onTap: () {
            addAnonimousSubTopic(viewModel, PointType.TEXT);
            print("Text Point Add Click");
          },
        ),
        SpeedDialChild(
          child: Icon(
            Icons.perm_media,
            color: Colors.amber,
          ),
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          label: 'Media Point',
          labelStyle: TextStyle(
            fontSize: 18.0,
            color: Colors.black,
          ),
          onTap: () {
            addAnonimousSubTopic(viewModel, PointType.MEDIA);
            print("Media Point Add Click");
          },
        ),
      ],
    );
  }

  void showSubTopicTitleDislog(DisplayNoteViewModel viewModel) {
    showDialog(
      context: context,
      child: SingleInputDialog(
        dialogFor: "SUB TOPIC",
        onOkPressed: (subTopicName) {
          viewModel.addSubTopic(subTopicName);
        },
        validate: (val) {
          return true;
        },
      ),
    );
  }

  void updateSelectedTopicId(String topicId) {
    setState(() {
      selectedTopicId = topicId;
    });
  }

  addAnonimousSubTopic(DisplayNoteViewModel viewModel, PointType type) async {
    NotePoint notePoint = NotePoint();
    switch (type) {
      case PointType.TEXT:
        notePoint = NotePoint(
          attachments: [],
          id: "anonymouspoint1",
          pointType: type,
          text: "",
        );
        break;
      case PointType.MEDIA:
        try {
          File mediaFile = await FilePicker.getFile(
            type: FileType.any,
          );
          notePoint = NoteService.validateMediaPoint(null, mediaFile);
        } catch (e) {
          print("File Selection Cancelled : " + e.toString());
        }
        break;
      default:
        return;
    }
    notePoint.pointType = type;
    viewModel.addAnonymousSubTopic(notePoint);
  }

  Future<bool> onBackPress(DisplayNoteViewModel viewModel) async {
    return true;
  }
}
