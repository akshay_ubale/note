import 'dart:io';

import 'package:note/models/note_point.dart';
import 'package:note/models/note_sub_topic.dart';
import 'package:note/redux/actions/actions.dart';
import 'package:note/redux/actions/note_actions.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:note/widgets/childrens/note/services/note_service.dart';
import 'package:redux/redux.dart';

class DisplayNoteViewModel {
  final String noteId;
  final String noteName;
  final List<NoteSubTopic> noteSubTopics;
  final bool changed;
  final Function errorShown;
  final Function(String, String) addNewTextPoint;
  final Function(String, List<NotePoint>) updateNotePoints;
  final Function(String, String, File) addMediaPoint;
  final Function(String) addSubTopic;
  final Function syncNote;
  final Function(NotePoint) addAnonymousSubTopic;

  DisplayNoteViewModel({
    this.noteId,
    this.noteName,
    this.noteSubTopics,
    this.changed,
    this.errorShown,
    this.addNewTextPoint,
    this.updateNotePoints,
    this.addMediaPoint,
    this.addSubTopic,
    this.syncNote,
    this.addAnonymousSubTopic,
  });

  factory DisplayNoteViewModel.create(Store<NoteState> store) {
    void _addNewTextPoint(String topicId, String selectedId) {
      NoteService.addTextPoint(store, topicId, selectedId);
    }

    void _updateNotePoints(String topicId, List<NotePoint> notePoints) {
      store.dispatch(UpdateNotePoints(topicId, notePoints));
    }

    void _addMediaPoint(String topicId, String selectedId, File mediaFile) {
      NotePoint notePoint = NoteService.validateMediaPoint(store, mediaFile);
      if (notePoint != null) {
        store.dispatch(AddPointSuccess(topicId, selectedId, notePoint));
      }
    }

    void _errorShown() {
      store.dispatch(RemoveError());
    }

    void _addSubTopic(String subTopicName) {
      NoteService.addSubTopic(store, subTopicName);
    }

    void _syncNoteToDB() {
      NoteService.syncNoteToDB(store);
    }

    void addAnonymousSubTopic(NotePoint notePoint) {
      NoteService.addAnonymousSubTopic(store, notePoint);
    }

    return DisplayNoteViewModel(
      noteId: store.state.currentNoteState.note?.noteId ?? "",
      noteName: store.state.currentNoteState.note?.noteName ?? "",
      noteSubTopics: store.state.currentNoteState.note?.noteSubTopics ?? [],
      changed: store.state.currentNoteState.changed,
      addNewTextPoint: _addNewTextPoint,
      updateNotePoints: _updateNotePoints,
      addMediaPoint: _addMediaPoint,
      errorShown: _errorShown,
      addSubTopic: _addSubTopic,
      syncNote: _syncNoteToDB,
      addAnonymousSubTopic: addAnonymousSubTopic,
    );
  }

  bool operator ==(old) => !changed && noteId == old.noteId;

  @override
  int get hashCode =>
      noteId.hashCode ^
      noteName.hashCode ^
      noteSubTopics.hashCode ^
      changed.hashCode;
}
