import 'dart:io';
import 'dart:math';

import 'package:mime/mime.dart';
import 'package:note/datastore/note_data_store.dart';
import 'package:note/datastore/note_data_store_model.dart';
import 'package:note/models/attachment.dart';
import 'package:note/models/note.dart';
import 'package:note/models/note_metadata.dart';
import 'package:note/models/note_point.dart';
import 'package:note/models/note_sub_topic.dart';
import 'package:note/redux/actions/note_actions.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:redux/redux.dart';

class NoteService {
  static void addTextPoint(
      Store<NoteState> store, String topicId, String selectedId) {
    NotePoint newNotePoint = new NotePoint(
      id: "note${Random().nextInt(200)}",
      attachments: [],
      pointType: PointType.TEXT,
      text: "",
    );
    store.dispatch(
      AddPointSuccess(
        topicId,
        selectedId,
        newNotePoint,
      ),
    );
  }

  static void addSubTopic(Store<NoteState> store, String topicName) {
    NoteSubTopic topic = NoteSubTopic(
      topicId: getUniqueId(topicName),
      subTopicName: topicName,
      notePoints: List(),
    );

    store.dispatch(AddSubTopicSuccess(topic));
  }

  static List<String> fileExtensions = ['pdf'];
  static NotePoint validateMediaPoint(Store<NoteState> store, File file) {
    List<String> mime = lookupMimeType(file.path).split('/');
    print('MIME : ' + mime[0] + " " + mime[1]);
    Attachment attachment;
    switch (mime[0]) {
      case 'image':
        attachment =
            Attachment(attachmentType: AttachmentType.IMAGE, uri: file.path);
        break;
      case 'application':
        if (fileExtensions.contains(mime[1])) {
          attachment =
              Attachment(attachmentType: AttachmentType.FILE, uri: file.path);
        }
        break;
    }
    if (attachment != null) {
      NotePoint notePoint = NotePoint(
        attachments: [attachment],
        id: "note${Random().nextInt(2000)}",
        text: "",
        pointType: PointType.MEDIA,
      );
      return notePoint;
    } else {
      print('File Type not supported.');
      return null;
    }
  }

  static void loadNotes(Store<NoteState> store, NoteMetadata noteMetadata) {
    try {
      NoteDataStoreModel noteDataStoreModel = NoteDataStore().readStore();

      store.dispatch(
        LoadNoteSuccess(
          noteMetadata,
          Note(
            noteId: noteMetadata.noteId,
            noteName: noteMetadata.noteName,
            noteSubTopics: noteDataStoreModel.noteSubTopics ?? [],
          ),
        ),
      );
    } catch (e) {
      print(e);
    }
  }

  static void syncNoteToDB(Store<NoteState> store) {
    try {
      NoteDataStore().writeStore(
        NoteDataStoreModel(
            noteSubTopics: store.state.currentNoteState.note.noteSubTopics),
      );

      store.dispatch(ResetChanged());
    } catch (e) {
      print("Note Saving : " + e.toString());
    }
  }

  static void addAnonymousSubTopic(
      Store<NoteState> store, NotePoint notePoint) {
    NoteSubTopic noteSubTopic = NoteSubTopic(
      notePoints: [notePoint],
      subTopicName: "",
      topicId: getUniqueId("Anonymous"),
    );

    store.dispatch(AddSubTopicSuccess(noteSubTopic));
  }

  static String getUniqueId(String text) {
    return text.replaceAll(' ', '') +
        "AK" +
        DateTime.now().millisecondsSinceEpoch.toString() +
        "SH" +
        Random().nextInt(1000).toString();
  }
}
