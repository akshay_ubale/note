import 'dart:io';
import 'dart:ui';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:note/Constants/contants.dart';
import 'package:note/models/attachment.dart';
import 'package:note/models/note_point.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:note/widgets/features/widgets/icon_tray.dart';
import 'package:note/widgets/features/widgets/marquee_text.dart';
import 'package:note/widgets/childrens/note/containers/file/file_container.dart';
import 'package:note/widgets/childrens/note/containers/image/image_container.dart';
import 'package:note/widgets/childrens/note/containers/point/point_container_viewmodel.dart';
import 'package:note/widgets/childrens/note/containers/text/text_container.dart';
import 'package:reorderables/reorderables.dart';

class SubTopicWidget extends StatefulWidget {
  final String topicId;
  final List<NotePoint> childrens;
  final String topicName;
  final bool reading;
  final Function(String, String) addNewTextPoint;
  final Function(String, List<NotePoint>) updateNotePoints;
  final Function(String, String, File) addMediaPoint;

  SubTopicWidget({
    @required this.childrens,
    this.topicId,
    this.topicName,
    this.reading,
    this.addNewTextPoint,
    this.updateNotePoints,
    this.addMediaPoint,
  });

  @override
  _SubTopicWidgetState createState() => _SubTopicWidgetState();
}

class _SubTopicWidgetState extends State<SubTopicWidget> {
  List<NotePoint> notePoints;
  bool _expanded;
  String _selectedId;

  @override
  void initState() {
    super.initState();
    _expanded =
        widget.reading ? true : widget.topicName.isNotEmpty ? false : true;
    _selectedId = "";
    notePoints = widget.childrens;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 5.0),
      color: Colors.white,
      child: Column(
        children: <Widget>[
          widget.topicName.isNotEmpty ? getTitleBar(context) : new Container(),
          (_expanded ? getReOrderableList(context) : Container()),
        ],
      ),
    );
  }

  Widget getTitleBar(BuildContext context) {
    return GestureDetector(
      onTap: () {
        toggleExpanded();
      },
      child: Container(
        height: 40.0,
        width: MediaQuery.of(context).size.width,
        color: Constants.subTitleColor,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                width: _expanded
                    ? MediaQuery.of(context).size.width - 100
                    : MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(left: 5.0),
                child: MarqueeWidget(
                  child: Text(
                    widget.topicName,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
            ),
            _expanded
                ? IconTray(
                    icons: [
                      IconButton(
                        icon: Icon(Icons.text_fields),
                        onPressed: () {
                          widget.addNewTextPoint(widget.topicId, _selectedId);
                        },
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                      ),
                      IconButton(
                        icon: Icon(Icons.perm_media),
                        onPressed: () {
                          getMediaFromInternalStorage();
                        },
                        splashColor: Colors.transparent,
                        highlightColor: Colors.transparent,
                      ),
                    ],
                  )
                : Container(),
          ],
        ),
      ),
    );
  }

  getMediaFromInternalStorage() async {
    try {
      File mediaFile = await FilePicker.getFile(
        type: FileType.any,
      );
      widget.addMediaPoint(widget.topicId, _selectedId, mediaFile);
    } catch (e) {
      print("File Selection Cancelled : " + e.toString());
    }
  }

  Widget getReOrderableList(BuildContext context) {
    return Container(
      color: Constants.notePointBackColor,
      child: ReorderableWrap(
        children: getPointWidgets(),
        onReorder: rearrangeChildrens,
      ),
    );
  }

  void updateSelectedKey(String selectedId) {
    setState(() {
      if (_selectedId == selectedId) {
        _selectedId = '';
      } else {
        _selectedId = selectedId;
      }
    });
  }

  void toggleExpanded() {
    setState(() {
      _expanded = !_expanded;
    });
  }

  void rearrangeChildrens(int oldIndex, int newIndex) {
    if (oldIndex != newIndex) {
      if (newIndex >= widget.childrens.length) {
        newIndex = widget.childrens.length - 1;
      }
      widget.childrens.insert(newIndex, widget.childrens.removeAt(oldIndex));

      widget.updateNotePoints(widget.topicId, widget.childrens);
      //call method which will dispatch update list action
    }
  }

  Widget getPointWidget(NotePoint notePoint) {
    return StoreConnector<NoteState, PointContainerViewModel>(
      converter: (store) =>
          PointContainerViewModel.create(store, widget.topicId, notePoint),
      builder: (context, vm) {
        switch (vm.notePoint.pointType) {
          case PointType.TEXT:
            return TextContainer(
              key: Key(vm.notePoint.id),
              id: notePoint.id,
              onSelect: updateSelectedKey,
              selected: vm.notePoint.id == _selectedId,
              onDelete: vm.deletePoint,
              notePoint: vm.notePoint,
              savePoint: vm.updatePoint,
              reading: widget.reading,
            );
          case PointType.MEDIA:
            if (vm.notePoint.attachments[0].attachmentType ==
                AttachmentType.IMAGE) {
              return ImageContainer(
                key: Key(vm.notePoint.id),
                id: vm.notePoint.id,
                selected: vm.notePoint.id == _selectedId,
                onSelect: updateSelectedKey,
                onDelete: vm.deletePoint,
                savePoint: vm.updatePoint,
                notePoint: vm.notePoint,
                reading: widget.reading,
              );
            } else {
              return FileContainer(
                key: Key(vm.notePoint.id),
                id: vm.notePoint.id,
                selected: vm.notePoint.id == _selectedId,
                onSelect: updateSelectedKey,
                onDelete: vm.deletePoint,
                notePoint: vm.notePoint,
                savePoint: vm.updatePoint,
                reading: widget.reading,
              );
            }
            break;
          default:
            return Container();
        }
      },
    );
  }

  List<Widget> getPointWidgets() {
    return [
      ...widget.childrens.map((notePoint) {
        return getPointWidget(notePoint);
      }).toList()
    ];
  }
}
