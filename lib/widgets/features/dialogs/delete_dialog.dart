import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DeleteDialog extends StatelessWidget {
  final String confirmationText;
  final Function onOK;

  DeleteDialog({@required this.confirmationText, @required this.onOK});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(confirmationText),
      actions: <Widget>[
        RaisedButton(
          child: Text("YES"),
          onPressed: () {
            onOK();
            Navigator.of(context).pop();
          },
        ),
        RaisedButton(
          child: Text("NO"),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
