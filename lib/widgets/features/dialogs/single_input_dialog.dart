import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SingleInputDialog extends StatefulWidget {
  final String dialogFor;
  final String okText;
  final String cancelText;
  final String value;
  final Function(String) onOkPressed;
  final Function(String) validate;

  SingleInputDialog({
    @required this.dialogFor,
    this.okText,
    this.cancelText,
    this.value,
    @required this.onOkPressed,
    this.validate,
  });

  @override
  _SingleInputDialogState createState() => _SingleInputDialogState();
}

class _SingleInputDialogState extends State<SingleInputDialog> {
  final _inputKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController(text: widget.value ?? "");
    return Dialog(
      child: Container(
        height: 150.0,
        child: Stack(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  left: 20.0, top: 10.0, right: 20.0, bottom: 0.0),
              child: Container(
                child: Form(
                  key: _inputKey,
                  child: TextFormField(
                    controller: controller,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Please Enter a Value";
                      }

                      if (!widget.validate(value)) {
                        return "${widget.dialogFor} already exists.";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      hintText: 'Enter ${widget.dialogFor.toLowerCase()} name',
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: 20.0,
                bottom: 10.0,
              ),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: RaisedButton(
                  onPressed: () {
                    if (_inputKey.currentState.validate()) {
                      widget.onOkPressed(controller.text);
                      Navigator.of(context).pop();
                    }
                  },
                  child: Text(
                    "ADD",
                    style: TextStyle(
                      backgroundColor: Colors.transparent,
                    ),
                  ),
                  color: Colors.transparent,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                right: 20.0,
                bottom: 10.0,
              ),
              child: Align(
                alignment: Alignment.bottomRight,
                child: RaisedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    "CANCEL",
                    style: TextStyle(
                      backgroundColor: Colors.transparent,
                    ),
                  ),
                  color: Colors.transparent,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
