import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note/models/note_metadata.dart';
import 'package:note/widgets/childrens/explorer/folder/folder_widget.dart';
import 'package:note/widgets/features/widgets/marquee_text.dart';

class NoteMoveDialog extends StatefulWidget {
  final NoteMetadata noteMetadata;
  final List<String> paths;
  final String activePath;
  final Function(String, NoteMetadata) onMove;

  NoteMoveDialog({
    this.noteMetadata,
    this.paths,
    this.activePath,
    this.onMove,
  });

  @override
  NoteMoveDialogState createState() => NoteMoveDialogState();
}

class NoteMoveDialogState extends State<NoteMoveDialog> {
  List<String> paths;
  String activePath;

  @override
  void initState() {
    super.initState();
    paths = widget.paths;
    activePath = widget.activePath;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        height: 500,
        child: Column(
          children: <Widget>[
            Container(
              height: 50,
              padding: EdgeInsets.only(bottom: 10.0),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    width: 1.0,
                    color: Colors.white,
                  ),
                ),
              ),
              child: Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      setState(() {
                        activePath = activePath.substring(
                            0, activePath.lastIndexOf("."));
                      });
                    },
                  ),
                  MarqueeWidget(
                    child: Text(activePath.replaceAll(".", " > ")),
                  ),
                ],
              ),
            ),
            Container(
              height: 300,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  children: getChildElements(),
                ),
              ),
            ),
            Container(
                height: 50.0,
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 10.0),
                      child: RaisedButton(
                        child: Text("MOVE HERE"),
                        onPressed: () {
                          widget.onMove(activePath, widget.noteMetadata);
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Spacer(),
                    Padding(
                      padding: EdgeInsets.only(right: 10.0),
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("CANCEL"),
                      ),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  List<Widget> getChildElements() {
    return paths
        .where((path) => path.startsWith(activePath) && path != activePath)
        .map((path) {
          String removeActive = path.replaceFirst(activePath + ".", "");
          return removeActive.substring(
              0,
              removeActive.contains(".")
                  ? removeActive.indexOf(".")
                  : removeActive.length);
        })
        .toSet()
        .toList()
        .map((folder) {
          return InkWell(
            onTap: () {
              setState(() {
                activePath = activePath + "." + folder;
              });
            },
            child: FolderWidget(
              folderName: folder,
              showAction: false,
            ),
          );
        })
        .toList();
  }
}
