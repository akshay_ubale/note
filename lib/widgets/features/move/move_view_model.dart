import 'package:note/models/note_metadata.dart';
import 'package:note/redux/actions/explorer_actions.dart';
import 'package:note/redux/state/app_state.dart';
import 'package:redux/redux.dart';

class NoteMoveViewModel {
  final String activePath;
  final List<String> paths;
  final Function(String, NoteMetadata) move;

  NoteMoveViewModel({this.activePath, this.move, this.paths});

  factory NoteMoveViewModel.create(Store<NoteState> store) {
    void _move(String newPath, NoteMetadata noteMetadata) {
      store.dispatch(MoveNoteToFolder(newPath, noteMetadata));
    }

    return NoteMoveViewModel(
      move: _move,
      activePath: store.state.explorerState.activePath,
      paths: store.state.explorerState.paths,
    );
  }
}
