import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note/widgets/features/widgets/file_path_line/path_name_shape_widget.dart';

class FileHistoryWidget extends StatefulWidget {
  final String path;

  final Function(String) onPress;

  FileHistoryWidget({this.path, this.onPress});

  @override
  FileHistoryWidgetState createState() => FileHistoryWidgetState();
}

class FileHistoryWidgetState extends State<FileHistoryWidget> {
  List<PathNameShapeWidget> allList;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30.0,
      child: Row(
        children: <Widget>[
          PathNameShapeWidget(
            width: 8.0,
            color: Colors.greenAccent,
            tillPath: ".NOTEROOTFOLDER",
            onPress: (path) {
              widget.onPress(path);
            },
          ),
          PathNameShapeWidget(
            width: 8.0,
            tillPath: ".NOTEROOTFOLDER",
            onPress: (path) {
              widget.onPress(path);
            },
          ),
          PathNameShapeWidget(
            width: 8.0,
            color: Colors.greenAccent,
            tillPath: ".NOTEROOTFOLDER",
            onPress: (path) {
              widget.onPress(path);
            },
          ),
          ...getPath(),
        ],
      ),
    );
  }

  List<PathNameShapeWidget> getPath() {
    print(widget.path);
    List<PathNameShapeWidget> widgets = [];
    String val = widget.path.replaceFirst(".NOTEROOTFOLDER", "");
    if (val.isEmpty) {
      return widgets;
    }
    List<String> splits = val.split(".");

    String tillPath = ".NOTEROOTFOLDER";
    double length = 24.0;
    for (String split in splits) {
      if (split.isEmpty) {
        continue;
      }
      tillPath += "." + split;

      widgets.add(
        PathNameShapeWidget(
          width: (split.length + 2) * 9.0,
          color: Colors.amber,
          text: split,
          tillPath: tillPath,
          onPress: (path) {
            widget.onPress(path);
          },
        ),
      );
      length += (split.length + 2) * 9.0;
      if (length > MediaQuery.of(context).size.width) {
        length -= widgets[0].width;
        widgets.removeAt(0);
      }
    }
    return widgets;
  }
}
