import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PathNameShapeWidget extends StatelessWidget {
  final double width;
  final Color color;
  final String text;
  final String tillPath;
  final Function(String) onPress;

  PathNameShapeWidget({
    @required this.width,
    this.color,
    this.text,
    this.onPress,
    this.tillPath,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onPress != null) {
          onPress(tillPath);
        }
      },
      child: ClipPath(
        child: Container(
          height: 20.0,
          width: width,
          color: color ?? Colors.grey,
          alignment: Alignment.center,
          child: Text(
            text ?? "",
            style: GoogleFonts.roboto(fontSize: 15.0),
          ),
        ),
        clipper: ShapeClipper(),
      ),
    );
  }
}

class ShapeClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = new Path();
    path.lineTo(0.0, 0.0);
    path.lineTo(size.width * 0.9, 0.0);
    path.lineTo(size.width, size.height * 0.5);
    path.lineTo(size.width * 0.9, size.height);
    path.lineTo(0, size.height);
    path.lineTo(size.width * 0.1, size.height * 0.5);
    path.lineTo(0.0, 0.0);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
