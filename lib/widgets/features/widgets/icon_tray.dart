import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum StartFrom { LEFT, RIGHT, TOP, BOTTOM }

enum Direction { HORIZONTAL, VERTICAL }

class IconTray extends StatefulWidget {
  final List<IconButton> icons;
  final StartFrom startFrom;
  final Direction direction;

  IconTray({
    this.icons,
    this.direction = Direction.HORIZONTAL,
    this.startFrom = StartFrom.RIGHT,
  });

  @override
  IconTrayState createState() => IconTrayState();
}

class IconTrayState extends State<IconTray> {
  bool hidden = true;

  @override
  void initState() {
    hidden = true;
    super.initState();
  }

  List<IconButton> getIcons() {
    List<IconButton> icons = <IconButton>[];

    if (hidden) {
      icons.addAll(widget.icons);
    }

    return icons;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: makeWidget(),
    );
  }

  Widget makeWidget() {
    List<Widget> childrens = [];

    Widget iconsRow = widget.direction == Direction.HORIZONTAL
        ? Row(
            children: widget.icons,
          )
        : Column(
            children: widget.icons,
          );

    Widget trayIcon = IconButton(
      icon: Icon(getIcon()),
      onPressed: () {
        setState(() {
          hidden = !hidden;
        });
      },
    );

    if (widget.startFrom == StartFrom.LEFT ||
        widget.startFrom == StartFrom.TOP) {
      childrens.add(trayIcon);
      if (!hidden) {
        childrens.add(iconsRow);
      }
    } else if (widget.startFrom == StartFrom.RIGHT ||
        widget.startFrom == StartFrom.BOTTOM) {
      if (!hidden) {
        childrens.add(iconsRow);
      }
      childrens.add(trayIcon);
    }

    return iconsRow = widget.direction == Direction.HORIZONTAL
        ? Row(
            children: childrens,
          )
        : Column(
            children: childrens,
          );
  }

  IconData getIcon() {
    switch (widget.startFrom) {
      case StartFrom.LEFT:
        return Icons.keyboard_arrow_right;
        break;
      case StartFrom.RIGHT:
        return Icons.keyboard_arrow_left;
        break;
      case StartFrom.TOP:
        return Icons.keyboard_arrow_down;
        break;
      case StartFrom.BOTTOM:
        return Icons.keyboard_arrow_up;
        break;
    }
    return Icons.menu;
  }
}
