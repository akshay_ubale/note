import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note/models/attachment.dart';

class ImageListWidget extends StatefulWidget {
  final List<Attachment> images;
  final Function() onImageAddition;
  final Function() onImageDeletion;

  ImageListWidget({this.images, this.onImageAddition, this.onImageDeletion});

  @override
  _ImageListWidgetState createState() => _ImageListWidgetState();
}

class _ImageListWidgetState extends State<ImageListWidget> {
  void showAlertForImageDeletetion(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Image Deletion Alert!!"),
          content: Text("Are you sure want to delete Image?"),
          actions: <Widget>[
            FlatButton(
              child: Text("YES"),
              onPressed: () {
                print("Call Delete Image Method");
                //widget.onImageDeletion();
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("NO"),
              onPressed: () {
                print("Do Nothing");
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  List<Container> getImageWidgets(BuildContext context) {
    return widget.images.map((model) {
      return Container(
        height: 100,
        width: 100,
        child: Padding(
          padding: EdgeInsets.all(5.0),
          child: GestureDetector(
            onVerticalDragEnd: (details) {
              print("on Drag Detected");
              showAlertForImageDeletetion(context);
            },
            child: Image.file(
              File(model.uri),
              fit: BoxFit.fill,
            ),
          ),
        ),
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.images == null || widget.images.length <= 0) {
      return Container();
    }

    return Container(
      height: 100.0,
      decoration: BoxDecoration(
        border: Border.symmetric(
          vertical: BorderSide(
            color: Colors.grey[50],
          ),
        ),
      ),
      child: Stack(
        children: <Widget>[
          ListView(
            scrollDirection: Axis.horizontal,
            children: getImageWidgets(context),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                color: Colors.grey[500].withOpacity(0.8),
              ),
              child: IconButton(
                onPressed: () {
                  print("Call On Addition Image");
                },
                icon: Icon(Icons.add_a_photo),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
